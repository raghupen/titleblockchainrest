package com.sofi.title.controllers;


import com.sofi.title.services.Web3jService;
import javafx.application.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import java.io.IOException;

@RestController
public class TitleController {

   @Autowired
   Web3jService web3jService;

    @RequestMapping(value = "/title", method = RequestMethod.GET,  produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getTitle() throws IOException {
       return web3jService.getClientVersion();

    }
    @RequestMapping(value = "/docs", method = RequestMethod.GET,  produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> getDocuments() throws IOException {
        ClassPathResource pdfFile = new ClassPathResource("pdf-sample.pdf");

        return ResponseEntity
                .ok()
                .contentLength(pdfFile.contentLength())
                .contentType(
                        MediaType.parseMediaType("application/octet-stream"))
                .body(new InputStreamResource(pdfFile.getInputStream()));

    }
}
