package com.sofi.title;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.Request;
import org.web3j.protocol.core.methods.response.Web3ClientVersion;

import java.io.IOException;

@SpringBootApplication
public class TitleBlockchainRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TitleBlockchainRestApplication.class, args);

	}

}
